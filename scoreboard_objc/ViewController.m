//
//  ViewController.m
//  scoreboard_objc
//
//  Created by Brad Roring on 9/22/17.
//  Copyright (c) 2017 Brad Roring. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label1;
- (IBAction)step1:(UIStepper *)sender;
@property (weak, nonatomic) IBOutlet UILabel *label2;
- (IBAction)step2:(UIStepper *)sender;
- (IBAction)reset:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIStepper *step1;
@property (weak, nonatomic) IBOutlet UIStepper *step2;


@end

@implementation ViewController

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)step1:(UIStepper *)sender {
    _label1.text = [NSString stringWithFormat:@"%.0f",sender.value];
}
- (IBAction)step2:(UIStepper *)sender {
    
    _label2.text = [NSString stringWithFormat:@"%.0f",sender.value];
}

- (IBAction)reset:(UIButton *)sender {
    
    _label1.text = @"0";
    _step1.value = 0;
    _label2.text = @"0";
    _step2.value = 0;
    
}
@end
